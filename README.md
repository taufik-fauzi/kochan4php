<p align="center">
  <img src="https://user-images.githubusercontent.com/69864986/212568928-10d27de9-15da-4837-b916-3428bb17adb0.gif" width="650">
</p>

### Hai 👋, Selamat datang di Profileku

```php
$myself = [
  'name' => 'Deo Subarno',
  'nickname' => 'Kochan',
  'pronouns' => 'He/Him',
  'skills' => ['HTML5', 'CSS3', 'JS', 'PHP', 'React.js', 'Next.js', 'Laravel', 'TailwindCSS', 'Bootstrap'],
  'architecture' => ['Monolithic', 'Microservice'],
  'funfact' => 'Otaku Programmer 😅',
  'OS' => 'Manjaro Linux'
];

echo $myself['OS'];
```
